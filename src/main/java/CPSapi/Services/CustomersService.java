package CPSapi.Services;

import CPSapi.entity.Customers;
import CPSapi.repository.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Service
public class CustomersService {

    @Autowired
    private final CustomersRepository customersRepository;


    public CustomersService(CustomersRepository customersRepository){
        this.customersRepository = customersRepository;
    }

    public void createCustomers(Customers customer){
        customersRepository.save(customer);
    }

    public List<Customers> getALL(){
        return customersRepository.findAll();
    }

    public Customers getById(Long ID){
        return customersRepository.findById(ID).orElse(null);
    }

    public void deleteCustomers(Long ID){
        customersRepository.deleteById(ID);
    }

    public void editCustomers(String title, boolean is_deleted, Timestamp created_at, Timestamp modified_at, Long ID){
        customersRepository.editCustomers(title,is_deleted,created_at,modified_at,ID);
    }
}
