package CPSapi.entity;

import CPSapi.entity.Customers;


import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name="products")

public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long ID;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Customers customer;
    @Column
    private String title;
    @Column
    private String description;
    @Column
    private double price;
    @Column
    private boolean is_deleted;
    @Column
    private Timestamp created_at;
    @Column
    private Timestamp modified_at;



    public void setID(Long ID) { this.ID = ID; }

    public void setCustomer_id(Customers customer) {
        this.customer = customer;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public void setModefied_at(Timestamp modified_at) {
        this.modified_at = modified_at;
    }

    public Long getID() {
        return ID;
    }

    public Customers getCustomer_id() {
        return customer;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public Timestamp getModefied_at() {
        return modified_at;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id = " + ID +
                ", customer_id = '" + customer + '\'' +
                ", title = '" + title + '\'' +
                ", description = '" + description + '\'' +
                ", price = '" + price + '\'' +
                ", is_deleted = '" + is_deleted + '\'' +
                ", create_at = " + created_at +'\''+
                ",modified_at = "+modified_at+
                '}';
    }
}
